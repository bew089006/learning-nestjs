
function statusOk(message:string = "" , data:any = {}) {
    return {
        statusCode: "OK",
        message: message,
        data: data
    }
}


export { statusOk }