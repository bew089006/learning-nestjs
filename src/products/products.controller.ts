import { Controller, 
  Get,
  Post,
  Request,
  UseInterceptors, 
  ClassSerializerInterceptor, 
  NotFoundException, 
  Body,
  Param,
  UseGuards,
  Patch,
  Delete
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { ProductDTO } from './products.dto';
import { JwtAuthGuard } from '../auth/jwt-auth.guard'

@Controller('products')
export class ProductsController {
  constructor(private productsService: ProductsService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Get()
  async getProducts() {
    let products = await this.productsService.findAll();
    return products
  } 

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Post()
  async createProduct(@Body() data: ProductDTO) {
    let product = await this.productsService.create(data);
    return product
  } 

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Get(":id")
  async findOne(@Param("id") id: number) {
    let product = await this.productsService.findOne(id);
    return product
  } 

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Patch(":id")
  async update(@Param("id") id: number, @Body() body: Partial<ProductDTO>) {
    let product = await this.productsService.update(id, body);
    return product
  } 

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Delete(":id")
  async delete(@Param("id") id: number) {
    if (await this.productsService.delete(id)) {
      return "Ok"
    } else {
      throw new NotFoundException('Not Found.');
    }
  } 
}
