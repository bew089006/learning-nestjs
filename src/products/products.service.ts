import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Product } from './product.entity';
import { ProductDTO } from './products.dto';
import { User } from '../users/user.entity';

@Injectable()
export class ProductsService {
  constructor(@InjectRepository(Product, "db2") private productRepository: Repository<Product>) {}

  async findAll() {
    return await this.productRepository.find();
  }

  async findOne(id: number) {
    return await this.productRepository.findOne({ id });
  }

  async create(data: ProductDTO) {
    return await this.productRepository.save({ ...data });
  }

  async update(id: number, data: Partial<ProductDTO>) {
    await this.productRepository.update({ id }, data)
    return this.findOne(id)
  }

  async delete(id: number) {
    if (await (await this.productRepository.delete({ id })).affected > 0) {
      return true
    }
    return false
  }
}
