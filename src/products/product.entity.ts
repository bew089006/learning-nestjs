import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('products')
export class Product {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({length: 25})
  name: string;

  @Column()
  price: number;

  constructor(partial: Partial<Product>) {
    Object.assign(this, partial);
  }
}