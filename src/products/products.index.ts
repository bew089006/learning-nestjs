import { Product } from './product.entity';
import { ProductsModule } from './products.module';

export { Product, ProductsModule }