import { IsEmail, IsNotEmpty, IsString, IsOptional, IsInt } from 'class-validator';

export class ProductDTO {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsInt()
  price: number;
}