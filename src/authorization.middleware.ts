import { Injectable, NestMiddleware } from '@nestjs/common';

@Injectable()
export class AuthorizationMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    // console.log("request....", req.headers);
    // console.log("response....", res);
    next();
  }
}
