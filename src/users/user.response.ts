import { Exclude, Type } from 'class-transformer';
import { User } from './user.entity';

export class UserResponseSnippet {
  id: number;

  @Exclude()
  username:string;

  @Exclude()
  password:string;

  firstname:string;

  lastname:string;

  constructor(partial: Partial<UserResponseSnippet>) {
    Object.assign(this, partial);
  }
}

export class UsersResponse {
  @Type(() => UserResponseSnippet)
  users: User[]   

  constructor() { }
}

export class UserResponse {
  @Type(() => UserResponseSnippet)
  user: User 

  constructor() { }
}