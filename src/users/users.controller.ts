import { 
  Controller, 
  Post, 
  Body, 
  Get, 
  Patch, 
  Delete, 
  Param, 
  Request, 
  ClassSerializerInterceptor, 
  UseInterceptors 
} from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersDTO } from './users.dto';
import { UsersResponse, UserResponse } from './user.response';

@Controller('users')
export class UsersController {
  constructor(private service: UsersService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get()
  async getUsers() {
    const users = await this.service.findAll();
    let response = new UsersResponse();
    response.users = users
    return response;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Post()
  async postUser(@Body() body: UsersDTO) {
    const user = await this.service.createUser(body);
    let response = new UserResponse();
    response.user = user
    return response;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Get(":id") 
  async getUserById(@Param("id") id: number) {
    const user = await this.service.findById(id);
    let response = new UserResponse();
    response.user = user
    return response
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Patch(":id") 
  async updateUserById(@Param("id") id: number, @Body() data: Partial<UsersDTO>) {
    const user = await this.service.updateUser(id, data);
    let response = new UserResponse();
    response.user = user
    return response;
  }

  @Delete(":id")
  async deleteUser(@Param("id") id: number) {
    this.service.deleteUser(id);
    return {
      message: "deleleted success"
    }
  }
}
