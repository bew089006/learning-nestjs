import { Injectable, Inject } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';
import { UsersDTO } from './users.dto';
import  * as bcrypt from 'bcrypt';
import { saltRounds } from './bcrypt_config';

@Injectable()
export class UsersService {
  constructor(@InjectRepository(User) private usersRepository: Repository<User>) {}
  
  async findAll() {
    return await this.usersRepository.find();
  }

  async findByUsername(username: string): Promise<User> {
    return await this.usersRepository.findOne({ username }); 
  }
  
  async findById(id: number): Promise<User> {
    return await this.usersRepository.findOne({ id }); 
  }

  async createUser(data: UsersDTO) {
    const hash = await bcrypt.hash(data.password, saltRounds);
    data.password = hash
    const user = this.usersRepository.create(data)
    await this.usersRepository.save(data)
    return user;
  }

  async updateUser(id: number, data: Partial<UsersDTO>) {
    await this.usersRepository.update({ id }, data)
    const user = this.usersRepository.findOne({ id })

    return user;
  }

  async deleteUser(id: number) {
    this.usersRepository.delete({ id });
  }
}
