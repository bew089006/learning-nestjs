import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { UsersDTO } from './users.dto';

describe('UsersController', () => {
  let userController: UsersController;
  let spyService: UsersService;

  beforeEach(async () => {
    const userServiceProvider = {
      provide: UsersService,
      useFactory: () => ({
        findAll: jest.fn(() => [
          {
            firstname: "wattanan",
            lastname: "saekang"
          }
        ]),
        createUser: jest.fn(() => (
          {
            firstname: "test_creted",
            lastname: "test_creted"
          }
        ))
      })
    }

    const app: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersService, userServiceProvider],
    }).compile();

    userController = app.get<UsersController>(UsersController);
    spyService = app.get<UsersService>(UsersService);
  });

  it('GET /users', async() => {
    const expected = [
      {
        firstname: "wattanan",
        lastname: "saekang"
      }
    ]

    let response = await userController.getUsers()
    expect(response).toEqual({ users: expected });
    expect(spyService.findAll).toHaveBeenCalled;
    expect(response.users.length).toEqual(1);
  });

  it('POST /users', async() => {
    const user_params: UsersDTO = {
      firstname: "test_creted",
      lastname: "test_creted",
      username: "test_creted",
      password: "123456"
    }
    let response = await userController.postUser(user_params) 

    const expected = {
      firstname: user_params.firstname,
      lastname: user_params.lastname,
    }

    expect(response).toEqual({ user: expected });
  });
});