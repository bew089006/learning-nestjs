import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Contact } from "../contacts/contact.entity"; 
import { Exclude } from 'class-transformer';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({length: 25})
  @Exclude()
  username:string;

  @Column()
  @Exclude()
  password:string;

  @Column({length: 25})
  firstname:string;

  @Column({length: 25})
  lastname:string;

  @OneToMany(type => Contact, contact => contact.user) 
  contacts: Contact[];

  constructor(partial: Partial<User>) {
    Object.assign(this, partial);
  }
}
