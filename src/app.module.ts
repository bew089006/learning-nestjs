import { Module, NestModule, MiddlewareConsumer  } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthService } from './auth/auth.service';
import { AuthModule } from './auth/auth.module';
import { Product, ProductsModule } from './products/products.index';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { LoggingInterceptor } from './logging.interceptor';
import { TransformInterceptor } from './transform.interceptor'
import { User, UsersModule } from './users/users.index';
import { Contact, ContactsModule } from './contacts/contacts.index';
import { AuthorizationMiddleware } from './authorization.middleware';
import { RouterModule } from 'nest-router';
import { routes } from './app.routes';

require('dotenv').config()

console.log("check db_host", process.env.DB_MYSQL_HOST);
@Module({
  imports: [
    RouterModule.forRoutes(routes),
    TypeOrmModule.forRoot({
      "type": "mysql",
      "host": process.env.DB_MYSQL_HOST,
      "port": ( +process.env.DB_MYSQL_PORT || 3306),
      "username": process.env.DB_MYSQL_USERNAME,
      "password": process.env.DB_MYSQL_PASSWORD,
      "database": process.env.DB_MYSQL_NAME,
      "synchronize": true,
      "entities": [User, Contact]
    }),
    TypeOrmModule.forRoot({
      "type": "postgres",
      "name": "db2",
      "host": process.env.DB_POSTGRES_HOST,
      "port": ( +process.env.DB_POSTGRES_PORT || 5432),
      "username": process.env.DB_POSTGRES_USERNAME,
      "password": process.env.DB_POSTGRES_PASSWORD,
      "database": process.env.DB_POSTGRES_NAME,
      "synchronize": true,
      "entities": [Product],
    }),
    UsersModule,
    AuthModule,
    ContactsModule,
    ProductsModule,
  ],
  controllers: [AppController],
  providers: [AppService, AuthService,
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthorizationMiddleware)
      .forRoutes('*');
  }
}
