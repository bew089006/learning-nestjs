import { 
  Controller, 
  Request, 
  Body, 
  Get, 
  Post, 
  Patch, 
  UseGuards, 
  ClassSerializerInterceptor, 
  UseInterceptors  
} from '@nestjs/common';
import { JwtAuthGuard } from './jwt-auth.guard'
import { LocalAuthGuard } from './local-auth.guard';
import { AuthService } from './auth.service';
import { UsersDTO } from '../users/users.dto';
import { statusOk } from '../../helpers/response_helper';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Patch('profile')
  async updateProfile(@Request() req, @Body() body: Partial<UsersDTO> ) {
    return await this.authService.updateProfile(req.user, body);
  }
}
