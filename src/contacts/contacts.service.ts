import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Contact } from './contact.entity'; 
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';
import { ContactDTO } from './contacts.dto';

@Injectable()
export class ContactsService {
  constructor(
    @InjectRepository(Contact) private contactRepository: Repository<Contact>,
    private usersService: UsersService
  ) {}

  async getContacts(user: User){
    const contacts = this.contactRepository.find({ user });
    return contacts;
  }

  async getContactById(id: number, user: User) {
    return this.contactRepository.findOne({user: user, id: id})
  }
  
  async createContact(user: User, data: ContactDTO) {
    const contact = await this.contactRepository.create({ user: user, ...data});
    this.contactRepository.save(contact)
    return contact ? contact : null
  }

  async updateContact(id: number, user: User, data: Partial<ContactDTO>) {
    await this.contactRepository.update({user: user, id: id}, data)
    return  this.contactRepository.findOne({user: user, id: id})
  }
  
  async deleteContact(id: number, user: User) {
    await this.contactRepository.delete({user: user, id: id})
  }
}