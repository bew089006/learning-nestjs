import { 
  Controller, 
  Get, 
  Delete,
  UseGuards, 
  Request, 
  Post, 
  Body, 
  NotFoundException, 
  Patch, 
  Param,
  ClassSerializerInterceptor, 
  UseInterceptors  
} from '@nestjs/common';
import { LocalAuthGuard } from '../auth/local-auth.guard';
import { ContactsService } from './contacts.service';
import { statusOk } from '../../helpers/response_helper';
import { JwtAuthGuard } from '../auth/jwt-auth.guard'
import { ContactDTO } from './contacts.dto';
import { UsersService } from '../users/users.service';
import { request } from 'express';
import { ContactsResponse, ContactResponse } from './contact.response';

@Controller('contacts')
export class ContactsController {
  constructor(private contactService: ContactsService,
     private userService: UsersService
  ) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Get()
  async getContact(@Request() req) {
    const contacts = await this.contactService.getContacts(req.user);
    let response = new ContactsResponse();
    response.contacts = contacts
    return response;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Post()
  async postUserContact(@Request() req, @Body() body: ContactDTO) {
    const contact = await this.contactService.createContact(req.user, body);
    
    if (contact) {
      let response = new ContactResponse();
      response.contact = contact

      return response;
    } else {
      throw new NotFoundException('Not Found.');
    }
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Get(":id")
  async getContactById(@Param("id") id: number, @Request() req) {
    const contact = await this.contactService.getContactById(id, req.user)
    let response = new ContactResponse();
    response.contact = contact
    return response;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Patch(":id")
  async updateContact(@Request() req: any, @Body() data: Partial<ContactDTO>, @Param("id") id: number) {
    const contact = await this.contactService.updateContact(id, req.user, data)
    let response = new ContactResponse();
    response.contact = contact
    return response;
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @UseGuards(JwtAuthGuard)
  @Delete(":id")
  async deleteContact(@Request() req: any, @Param("id") id: number) {
    await this.contactService.deleteContact(id, req.user)
    return statusOk("contact deleted")
  }
}
