import { Exclude, Type } from 'class-transformer';
import { Contact } from './contact.entity';
import { User } from "../users/user.entity";

export class ContactSnippetResponse {
  @Exclude()
  id: number

  firstname: string

  lastname: string

  email: string

  tel: string

  @Exclude()
  user: User;

  constructor(partial: Partial<Contact>) {
    Object.assign(this, partial);
  }
}

export class ContactsResponse {
  @Type(() => ContactSnippetResponse)
  contacts: Contact[] 

  constructor() { }
}

export class ContactResponse {
  @Type(() => ContactSnippetResponse)
  contact: Contact

  constructor() { }
}