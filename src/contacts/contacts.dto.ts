import { IsEmail, IsNotEmpty, IsString, IsOptional } from 'class-validator';

export class ContactDTO {
  @IsNotEmpty()
  @IsString()
  firstname: string;

  @IsNotEmpty()
  @IsString()
  lastname: string;

  @IsEmail()
  @IsString()
  email: string;

  @IsString()
  @IsOptional()
  tel: string;
}