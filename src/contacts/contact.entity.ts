import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { User } from "../users/user.entity";
import { Exclude } from 'class-transformer';

@Entity()
export class Contact {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 25 })
  firstname: string;

  @Column({ length: 25 })
  lastname: string;

  @Column({ length: 50 })
  email: string;

  @Column({ length: 10 , nullable: true})
  tel: string;

  @ManyToOne(type => User, user => user.contacts) 
  user: User;

  constructor(partial: Partial<Contact>) {
    Object.assign(this, partial);
  }
}

            