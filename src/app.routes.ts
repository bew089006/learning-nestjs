import { Routes } from 'nest-router';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ContactsModule } from './contacts/contacts.module';
import { ProductsModule } from './products/products.module';

export const routes: Routes = [
  {
    path: 'api/v1/learning',
    children: [
      AuthModule,
      UsersModule,
      ContactsModule,
      ProductsModule
    ]
  }
]