FROM node:12.13-alpine

WORKDIR /usr/src/app

RUN npm install -g @nestjs/cli

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

COPY /dist ./dist

CMD ["node", "dist/main"]